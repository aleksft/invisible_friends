FROM ruby:3.1.2-buster

# throw errors if Gemfile has been modified since Gemfile.lock
# RUN bundle config --global frozen 1

RUN apt update && \
    apt upgrade -y && \
    apt install sqlite3 -y && \
    apt install nodejs npm -y

WORKDIR /usr/src/app

RUN apt install gawk -y && \
    apt install bison -y && \
    cd /tmp && \
    wget -4c https://ftp.gnu.org/gnu/glibc/glibc-2.29.tar.gz && \
    tar -zxvf glibc-2.29.tar.gz && \
    cd glibc-2.29 && \
    mkdir build_dir && \
    cd build_dir && \
    ../configure --prefix=/opt/glibc && \
    make && \
    make install && \
    cd /usr/src/app

COPY Gemfile Gemfile.lock ./
RUN bundle install

# COPY . .
COPY ./entrypoint.sh ./

EXPOSE 3000

RUN chmod 0777 /usr/src/app/entrypoint.sh
ENTRYPOINT ["/bin/sh", "/usr/src/app/entrypoint.sh"]
