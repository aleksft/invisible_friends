# RUN IN PRODUCTION

- Uncomment the "export RAILS_ENV=production" line in the entrypoint.sh file



# DOCKER

- Create docker image:
  $ docker build -t invisible_friends .

- Create docker container:
  $ docker create -v $(pwd):/usr/src/app -p 59834:3000 --name invisible_friends invisible_friends

- Run docker container:
  $ docker run -v $(pwd):/usr/src/app -p 59834:3000 --name invisible_friends invisible_friends

- Stop docker container:
  $ docker stop invisible_friends

- Start docker container:
  $ docker start invisible_friends

- Access docker container:
  $ docker exec -it invisible_friends /bin/bash

  $ rails c
  $ RAILS_ENV=production rails c



# RUN TEST (USING DOCKER)

- Create docker image, create docker container and start docker container

- Run the tests
  $ docker exec invisible_friends rspec



# APP

- Create game:
  $ curl --header "Content-Type: application/json" \
    --request POST \
    --data '{"game":{"name":"<game name>","people_links":"<number of gifts per person>","min_price":"<gift min price>","max_price":"<gift max price>","date":"<game date (dd/mm/yyyy)>"}}' \
    http://localhost:59834/api/v1/games

- Add participants (if the person participated in a previous game, it is not necessary to include the name unless you want to update it):
  $ curl --header "Content-Type: application/json" \
    --request POST \
    --data '{"game":{"participants":[{"name":"<person 1 name>","email":"<person 1 email>"},..,{"name":"<person N name>","email":"<person N email>"}]}}' \
    http://localhost:59834/api/v1/games/<game id>/participants

- Create assignments:
  $ curl --header "Content-Type: application/json" \
    --request POST \
    http://localhost:59834/api/v1/games/<game id>/assignments
