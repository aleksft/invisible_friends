class Api::V1::GamesController < ApplicationController
    before_action :set_game, only: [:show, :participants, :assignments]
  
    # GET /api/v1/games
    #-----------------------------------------------------------------------------
    def index
      render json: Game.all
    end
  
    # GET /api/v1/games/:id
    #-----------------------------------------------------------------------------
    def show
      render json: @game
    end
  
    # POST /api/v1/games
    #-----------------------------------------------------------------------------
    def create
      @game = Game.new(game_params)
  
      if @game.save
        render json: @game
      else
        unprocessable_entity_rescue(@game.errors)
      end
    end
  
    # PATCH/PUT /api/v1/games/:id
    #-----------------------------------------------------------------------------
    # def update
    #   if @game.update(game_params)
    #     render json: @game
    #   else
    #     unprocessable_entity_rescue(@game.errors)
    #   end
    # end
  
    # DELETE /api/v1/games/:id
    #-----------------------------------------------------------------------------
    # def destroy
    #   @game.destroy
    # end

    # POST /api/v1/games/:id/participants
    #-----------------------------------------------------------------------------
    def participants
      if @game.add_participants(participants_params)
        render json: @game
      else
        unprocessable_entity_rescue(@game.errors)
      end
    end

    # POST /api/v1/games/:id/assignments
    #-----------------------------------------------------------------------------
    def assignments
      if @game.assign
        @game.participants.each do |participant|
          PersonMailer.game_assignment_email(participant.person, @game).deliver_now
        end
        render json: @game
      else
        unprocessable_entity_rescue(@game.errors)
      end
    end
  
    private
    # Use callbacks to share common setup or constraints between actions.
    #-----------------------------------------------------------------------------
    def set_game
      @game = Game.find(params[:id])
    end
    
    # Only allow a trusted parameter "white list" through.
    #-----------------------------------------------------------------------------
    def game_params
      params.require(:game).permit(:name, :people_links, :min_price, :max_price, :date)
    end

    #-----------------------------------------------------------------------------
    def participants_params
      params.require(:game).permit(participants: [:name, :email])[:participants]
    end
  
  end