class ApplicationController < ActionController::API
    rescue_from ActiveRecord::RecordNotFound, with: :not_found_rescue
    rescue_from ActiveRecord::RecordInvalid, with: :unprocessable_entity_rescue
    rescue_from ActiveRecord::RecordNotSaved, with: :unprocessable_entity_rescue

    #-----------------------------------------------------------------------------
    def not_found_rescue(exception)
        render json: {
            error: exception
        },
        status: :not_found
    end

    #-----------------------------------------------------------------------------
    def unprocessable_entity_rescue(exception)
        render json: {
            error: exception
        },
        status: :unprocessable_entity
    end
end
