class ApplicationMailer < ActionMailer::Base
  default from: CONFIG['mailer']['from']
  layout "mailer"
end
