class PersonMailer < ApplicationMailer
    def game_assignment_email(person, game)
        @person = person
        @game = game
        mail(to: @person.email, subject: @game.name)
    end
end