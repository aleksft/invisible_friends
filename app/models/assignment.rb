class Assignment < ApplicationRecord
    validates :game, presence: true
    validates :person, presence: true
    validates :linked, presence: true
    validate :person_is_participant, :linked_is_participant

    belongs_to :game
    belongs_to :person
    belongs_to :linked, class_name: 'Person'

    #-----------------------------------------------------------------------------
    def person_is_participant
        is_participant = self.game.participants.select { |p| p.person_id == self.person_id }.any?
        errors.add(:person, "must be a participant") unless is_participant
    end

    #-----------------------------------------------------------------------------
    def linked_is_participant
        is_participant = self.game.participants.select { |p| p.person_id == self.linked_id }.any?
        errors.add(:linked, "must be a participant") unless is_participant
    end
end