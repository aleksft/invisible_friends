class Game < ApplicationRecord
    validates :name, presence: true
    validates :people_links, presence: true, numericality: {
        only_integer: true,
        greater_than_or_equal_to: 1
    }
    validates :min_price, presence: true, numericality: {
        only_integer: true,
        greater_than_or_equal_to: 1
    }
    validates :max_price, presence: true, numericality: {
        only_integer: true,
        greater_than_or_equal_to: ->(game) { game.min_price.blank? ? 0 : game.min_price }
    }
    validates :date, presence: true
    validate :valid_date
    validates_associated :people, on: :add_participants
    validates_associated :participants, on: :add_participants
    validates_associated :assignments, on: :assign
    
    has_many :people, through: :participants
    has_many :participants, dependent: :destroy
    has_many :assignments, dependent: :destroy

    #-----------------------------------------------------------------------------
    def add_participants(params)
        return unless self.no_assignments?

        params.each do |person_params|
            person = Person.find_by_email(person_params[:email])

            if person.blank?
                person = Person.create!(person_params)
            elsif !person_params[:name].blank?
                person.update_attribute('name', person_params[:name])
            end

            self.participants.new({person: person})
        end

        self.save!
    end

   #-----------------------------------------------------------------------------
   def assign
        return unless self.no_assignments?

        return unless self.can_assign?

        joins = {}
        people_ids = self.participants.pluck(:person_id)
        people_ids.map { |id| joins[id] = Array.new(self.people_links) }

        return unless (0..people_links-1).each do |index|
            break unless self.assignment_loop(0, index, people_ids, joins)
        end

        assignments_params = joins.map { |k, v| v.map { |i| [k, i] } }.flatten(1).map { |e| ['person_id', 'linked_id'].zip(e).to_h }
        assignments_params.each { |assignment_params| self.assignments.new(assignment_params) }
        self.save!
   end

   private
   #-----------------------------------------------------------------------------
   def valid_date
        errors.add(:date, "is not a date") unless self.date.instance_of?(Date)
    end

   #-----------------------------------------------------------------------------
   def no_assignments?
        ok = self.assignments.empty?

        errors.add(:base, "Already exists assignments") unless ok

        ok
    end

   #-----------------------------------------------------------------------------
   def can_assign?
        ok = self.participants.size > self.people_links

        errors.add(:base, "Participants links can't be greater or equal than number of people") unless ok

        ok
    end

    #-----------------------------------------------------------------------------
    def assignment_loop(retries, index, people_ids, joins)
        remaining_ids = people_ids.map { |i| i }

        success = people_ids.each do |person_id|
            possible_ids = remaining_ids.select { |id| !([person_id] + joins[person_id]).include?(id) }
            break if possible_ids.empty?
            selected_id = possible_ids[rand(possible_ids.size)]
            joins[person_id][index] = selected_id
            remaining_ids.delete(selected_id)
        end

        unless success
            if retries >= 10
                errors.add(:base, "Too much retries")
                return false
            end

            people_ids.each { |person_id| joins[person_id][index] = nil }
            success = self.assignment_loop(retries + 1, index, people_ids, joins)
        end

        success
    end
end