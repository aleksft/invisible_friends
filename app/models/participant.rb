class Participant < ApplicationRecord
    validates :game,
        presence: true,
        uniqueness: { case_sensitive: false, scope: :person }
    validates :person,
        presence: true,
        uniqueness: { case_sensitive: false, scope: :game }
    
    belongs_to :game
    belongs_to :person
end