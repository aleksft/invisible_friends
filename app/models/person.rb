class Person < ApplicationRecord
    validates :name, presence: true
    validates :email,
        presence: true,
        uniqueness: { case_sensitive: false },
        format: { with: URI::MailTo::EMAIL_REGEXP }
    validates :name,
        presence: true,
        uniqueness: { case_sensitive: false }
    
    has_many :games, through: :participants
    has_many :participants, dependent: :destroy
    has_many :assignments, dependent: :destroy
    has_many :links, class_name: 'Assignment', foreign_key: 'linked_id', dependent: :destroy
end