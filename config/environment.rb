# Load the Rails application.
require_relative "application"

CONFIG = YAML.load_file('config/application.yml')[Rails.env]

# Initialize the Rails application.
Rails.application.initialize!