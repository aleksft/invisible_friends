Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      resources :games, only: [:index, :show, :create] do
        member do
          post :participants
          post :assignments
        end
      end
    end
  end
end
