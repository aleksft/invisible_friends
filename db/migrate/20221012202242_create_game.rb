class CreateGame < ActiveRecord::Migration[7.0]
  def change
    create_table :games do |t|
      t.string :name
      t.integer :people_links

      t.timestamps
    end
  end
end
