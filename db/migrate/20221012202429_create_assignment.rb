class CreateAssignment < ActiveRecord::Migration[7.0]
  def change
    create_table :assignments do |t|
      t.references :game, null: false, foreign_key: true
      t.references :person, null: true, foreign_key: { to_table: 'people' }
      t.references :linked, null: true, foreign_key: { to_table: 'people' }

      t.timestamps
    end

    add_index :assignments, [:game_id, :person_id, :linked_id], unique: true
  end
end
