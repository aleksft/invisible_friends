class AddGamePrices < ActiveRecord::Migration[7.0]
  def change
    add_column :games, :min_price, :integer
    add_column :games, :max_price, :integer
  end
end
