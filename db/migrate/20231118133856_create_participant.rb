class CreateParticipant < ActiveRecord::Migration[7.0]
  def change
    create_table :participants do |t|
      t.references :game, null: false, foreign_key: true
      t.references :person, null: true, foreign_key: { to_table: 'people' }

      t.timestamps
    end

    add_index :participants, [:game_id, :person_id], unique: true
  end
end
