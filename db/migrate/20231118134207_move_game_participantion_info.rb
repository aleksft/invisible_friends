class MoveGameParticipantionInfo < ActiveRecord::Migration[7.0]
  def change
    Person.all.each do |person|
      Participant.create!(game_id: person.game_id, person_id: person.id)
    end
  end
end
