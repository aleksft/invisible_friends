class DeletePersonGameId < ActiveRecord::Migration[7.0]
  def change
    remove_column :people, :game_id
  end
end
