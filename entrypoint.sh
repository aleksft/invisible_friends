#!/usr/bin/env bash
echo "ENTRYPOINT"

# export RAILS_ENV=production

cd /usr/src/app

./bin/bundle exec rails db:create

./bin/bundle exec rails db:migrate

rm -f tmp/pids/server.pid

./bin/bundle exec rails s -p 3000 -b '0.0.0.0'