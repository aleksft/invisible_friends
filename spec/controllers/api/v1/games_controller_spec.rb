describe Api::V1::GamesController, type: :request do
    describe '#index' do
        before do
            create(:game)
            create(:game)
            create(:game)
        end

        it 'lists all games' do
            get '/api/v1/games'

            expect(response.status).to eq(200)
            expect(response.parsed_body.size).to eq(3)
        end
    end

    describe '#show' do
        before do
            @game = create(:game)
            @game_json = {
                'id' => @game.id,
                'name' => @game.name,
                'people_links' => @game.people_links,
                'min_price' => @game.min_price,
                'max_price' => @game.max_price,
                'date' => @game.date.to_s,
                'created_at' => @game.created_at.strftime('%Y-%m-%dT%H:%M:%S.%LZ'),
                'updated_at' => @game.updated_at.strftime('%Y-%m-%dT%H:%M:%S.%LZ')
            }
        end

        it 'shows a game' do
            get "/api/v1/games/#{@game.id}"

            expect(response.status).to eq(200)
            expect(response.parsed_body).to eq(@game_json)
        end

        it 'fails with wrong id' do
            get "/api/v1/games/-1"

            expect(response.status).to eq(404)
            expect(response.parsed_body['error']).to eq("Couldn't find Game with 'id'=-1")
        end
    end

    describe '#create' do
        it 'creates a game' do
            post '/api/v1/games', params: { game: { name: 'Game test 1', people_links: 1, min_price: 10, max_price: 20, date: Date.today } }

            expect(response.status).to eq(200)
            expect(Game.last.name).to eq('Game test 1')
            expect(Game.last.people_links).to eq(1)
        end

        it 'fails without name' do
            post '/api/v1/games', params: { game: { people_links: 1, min_price: 10, max_price: 20, date: Date.today } }

            expect(response.status).to eq(422)
            expect(response.parsed_body['error']['name'].size).to eq(1)
            expect(response.parsed_body['error']['name'][0]).to eq("can't be blank")
        end

        it 'fails without people_links' do
            post '/api/v1/games', params: { game: { name: 'Game test 2', min_price: 10, max_price: 20, date: Date.today } }

            expect(response.status).to eq(422)
            expect(response.parsed_body['error']['people_links'].size).to eq(2)
            expect(response.parsed_body['error']['people_links'][0]).to eq("can't be blank")
            expect(response.parsed_body['error']['people_links'][1]).to eq("is not a number")
        end

        it 'fails with text people_links' do
            post '/api/v1/games', params: { game: { name: 'Game test 2', people_links: 'number', min_price: 10, max_price: 20, date: Date.today } }

            expect(response.status).to eq(422)
            expect(response.parsed_body['error']['people_links'].size).to eq(1)
            expect(response.parsed_body['error']['people_links'][0]).to eq("is not a number")
        end

        it 'fails with no int people_links' do
            post '/api/v1/games', params: { game: { name: 'Game test 2', people_links: 10.4, min_price: 10, max_price: 20, date: Date.today } }

            expect(response.status).to eq(422)
            expect(response.parsed_body['error']['people_links'].size).to eq(1)
            expect(response.parsed_body['error']['people_links'][0]).to eq("must be an integer")
        end

        it 'fails with people_links lower than 1' do
            post '/api/v1/games', params: { game: { name: 'Game test 3', people_links: 0, min_price: 10, max_price: 20, date: Date.today } }

            expect(response.status).to eq(422)
            expect(response.parsed_body['error']['people_links'].size).to eq(1)
            expect(response.parsed_body['error']['people_links'][0]).to eq("must be greater than or equal to 1")
        end

        it 'fails without min_price' do
            post '/api/v1/games', params: { game: { name: 'Game test 2', people_links: 1, max_price: 20, date: Date.today } }

            expect(response.status).to eq(422)
            expect(response.parsed_body['error']['min_price'].size).to eq(2)
            expect(response.parsed_body['error']['min_price'][0]).to eq("can't be blank")
            expect(response.parsed_body['error']['min_price'][1]).to eq("is not a number")
        end

        it 'fails with text min_price' do
            post '/api/v1/games', params: { game: { name: 'Game test 2', people_links: 1, min_price: 'number', max_price: 20, date: Date.today } }

            expect(response.status).to eq(422)
            expect(response.parsed_body['error']['min_price'].size).to eq(1)
            expect(response.parsed_body['error']['min_price'][0]).to eq("is not a number")
        end

        it 'fails with no int min_price' do
            post '/api/v1/games', params: { game: { name: 'Game test 2', people_links: 1, min_price: 10.4, max_price: 20, date: Date.today } }

            expect(response.status).to eq(422)
            expect(response.parsed_body['error']['min_price'].size).to eq(1)
            expect(response.parsed_body['error']['min_price'][0]).to eq("must be an integer")
        end

        it 'fails with min_price lower than 1' do
            post '/api/v1/games', params: { game: { name: 'Game test 3', people_links: 1, min_price: 0, max_price: 20, date: Date.today } }

            expect(response.status).to eq(422)
            expect(response.parsed_body['error']['min_price'].size).to eq(1)
            expect(response.parsed_body['error']['min_price'][0]).to eq("must be greater than or equal to 1")
        end

        it 'fails without max_price' do
            post '/api/v1/games', params: { game: { name: 'Game test 2', people_links: 1, min_price: 10, date: Date.today } }

            expect(response.status).to eq(422)
            expect(response.parsed_body['error']['max_price'].size).to eq(2)
            expect(response.parsed_body['error']['max_price'][0]).to eq("can't be blank")
            expect(response.parsed_body['error']['max_price'][1]).to eq("is not a number")
        end

        it 'fails with text max_price' do
            post '/api/v1/games', params: { game: { name: 'Game test 2', people_links: 1, min_price: 10, max_price: 'number', date: Date.today } }

            expect(response.status).to eq(422)
            expect(response.parsed_body['error']['max_price'].size).to eq(1)
            expect(response.parsed_body['error']['max_price'][0]).to eq("is not a number")
        end

        it 'fails with text max_price' do
            post '/api/v1/games', params: { game: { name: 'Game test 2', people_links: 1, min_price: 10, max_price: 10.4, date: Date.today } }

            expect(response.status).to eq(422)
            expect(response.parsed_body['error']['max_price'].size).to eq(1)
            expect(response.parsed_body['error']['max_price'][0]).to eq("must be an integer")
        end

        it 'fails with max_price lower than min_price' do
            post '/api/v1/games', params: { game: { name: 'Game test 3', people_links: 1, min_price: 10, max_price: 9, date: Date.today } }

            expect(response.status).to eq(422)
            expect(response.parsed_body['error']['max_price'].size).to eq(1)
            expect(response.parsed_body['error']['max_price'][0]).to eq("must be greater than or equal to 10")
        end

        it 'fails without date' do
            post '/api/v1/games', params: { game: { name: 'Game test 2', people_links: 1, min_price: 10, max_price: 20 } }

            expect(response.status).to eq(422)
            expect(response.parsed_body['error']['date'].size).to eq(2)
            expect(response.parsed_body['error']['date'][0]).to eq("can't be blank")
            expect(response.parsed_body['error']['date'][1]).to eq("is not a date")
        end

        it 'fails with text max_price' do
            post '/api/v1/games', params: { game: { name: 'Game test 2', people_links: 1, min_price: 10, max_price: 20, date: 'date' } }

            expect(response.status).to eq(422)
            expect(response.parsed_body['error']['date'].size).to eq(2)
            expect(response.parsed_body['error']['date'][0]).to eq("can't be blank")
            expect(response.parsed_body['error']['date'][1]).to eq("is not a date")
        end
    end

    describe '#people' do
        before do
            @game = create(:game)

            @game_lock = create(:game, people_links: 1)
            @person1_lock = create(:person)
            @person2_lock = create(:person)
            @participant1_lock = create(:participant, game: @game_lock, person: @person1_lock)
            @participant2_lock = create(:participant, game: @game_lock, person: @person2_lock)
            create(:assignment, game: @game_lock, person: @person1_lock, linked: @person2_lock)
            create(:assignment, game: @game_lock, person: @person2_lock, linked: @person1_lock)
        end

        it 'adds people' do
            post "/api/v1/games/#{@game.id}/participants",
                params: {
                    game: {
                        participants: [
                            { name: 'Person 1', email: 'person1@example.com' },
                            { name: 'Person 2', email: 'person2@example.com' },
                            { name: 'Person 3', email: 'person3@example.com' },
                            { name: 'Person 4', email: 'person4@example.com' }
                        ]
                    }
                }
            
            expect(response.status).to eq(200)
            expect(@game.participants.count).to eq(4)
        end

        it 'fails without name' do
            post "/api/v1/games/#{@game.id}/participants", params: { game: { participants: [ { email: 'test@example.com' } ] } }

            expect(response.status).to eq(422)
            expect(response.parsed_body['error']).to eq("Validation failed: Name can't be blank")
        end

        it 'fails with duplicated name' do
            post "/api/v1/games/#{@game.id}/participants",
                params: {
                    game: {
                        participants: [
                            { name: 'Person', email: 'person1@example.com' },
                            { name: 'Person', email: 'person2@example.com' }
                        ]
                    }
                }

            expect(response.status).to eq(422)
            expect(response.parsed_body['error']).to eq("Validation failed: Name has already been taken")
        end

        it 'fails without email' do
            post "/api/v1/games/#{@game.id}/participants", params: { game: { participants: [ { name: 'Person' } ] } }

            expect(response.status).to eq(422)
            expect(response.parsed_body['error']).to eq("Validation failed: Email can't be blank, Email is invalid")
        end

        it 'fails with duplicated email' do
            post "/api/v1/games/#{@game.id}/participants",
                params: {
                    game: {
                        participants: [
                            { name: 'Person 1', email: 'person@example.com' },
                            { name: 'Person 2', email: 'person@example.com' }
                        ]
                    }
                }

            expect(response.status).to eq(422)
            expect(response.parsed_body['error']).to eq("Validation failed: Participants is invalid")
        end

        it 'fails with wrong email' do
            post "/api/v1/games/#{@game.id}/participants",
                params: {
                    game: {
                        participants: [
                            { name: 'Person', email: 'example.com' }
                        ]
                    }
                }

            expect(response.status).to eq(422)
            expect(response.parsed_body['error']).to eq("Validation failed: Email is invalid")
        end

        it 'fails when already exists assignments' do
            post "/api/v1/games/#{@game_lock.id}/participants",
                params: {
                    game: {
                        participants: [
                            { name: 'Person', email: 'person@example.com' }
                        ]
                    }
                }

            expect(response.status).to eq(422)
            expect(response.parsed_body['error']['base'].size).to eq(1)
            expect(response.parsed_body['error']['base'][0]).to eq("Already exists assignments")
        end
    end

    describe '#assignments' do
        before do
            @person1 = create(:person)
            @person2 = create(:person)

            @game1 = create(:game, people_links: 1)
            @participant1 = create(:participant, game: @game1, person: @person1)
            @participant2 = create(:participant, game: @game1, person: @person2)

            @game2 = create(:game, people_links: 2)
            create(:participant, game: @game2, person: @person1)
            create(:participant, game: @game2, person: @person2)

            @game_lock = create(:game, people_links: 1)
            @person1_lock = create(:person)
            @person2_lock = create(:person)
            @participant1_lock = create(:participant, game: @game_lock, person: @person1_lock)
            @participant2_lock = create(:participant, game: @game_lock, person: @person2_lock)
            create(:assignment, game: @game_lock, person: @person1_lock, linked: @person2_lock)
            create(:assignment, game: @game_lock, person: @person2_lock, linked: @person1_lock)
        end

        it 'assigns' do
            post "/api/v1/games/#{@game1.id}/assignments"

            expect(response.status).to eq(200)
            expect(@game1.assignments.size).to eq(2)

            expect(ActionMailer::Base.deliveries.size).to be >= 2
            expect(ActionMailer::Base.deliveries[-1].to).to eq([@person2.email])
            expect(ActionMailer::Base.deliveries[-1].subject).to eq(@game1.name)
            expect(ActionMailer::Base.deliveries[-2].to).to eq([@person1.email])
            expect(ActionMailer::Base.deliveries[-2].subject).to eq(@game1.name)
        end

        it 'fails' do
            post "/api/v1/games/#{@game2.id}/assignments"

            expect(response.status).to eq(422)
            expect(response.parsed_body['error']['base'].size).to eq(1)
            expect(response.parsed_body['error']['base'][0]).to eq("Participants links can't be greater or equal than number of people")
        end

        it 'fails when already exists assignments' do
            post "/api/v1/games/#{@game_lock.id}/assignments"

            expect(response.status).to eq(422)
            expect(response.parsed_body['error']['base'].size).to eq(1)
            expect(response.parsed_body['error']['base'][0]).to eq("Already exists assignments")
        end
    end
end