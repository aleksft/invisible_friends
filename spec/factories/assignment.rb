FactoryBot.define do
    factory :assignment do
        association :game
        association :person
        association :linked, factory: :person
    end
end