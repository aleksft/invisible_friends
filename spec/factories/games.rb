FactoryBot.define do
    factory :game do
        name { Faker::ChuckNorris.fact }
        people_links { Faker::Types.rb_integer(from: 1, to: 100) }
        min_price { Faker::Types.rb_integer(from: 1, to: 50) }
        max_price { Faker::Types.rb_integer(from: 51, to: 100) }
        date { Faker::Date.forward }
    end
end