FactoryBot.define do
    factory :participant do
        association :game
        association :person
    end
end