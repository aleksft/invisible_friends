FactoryBot.define do
    factory :person do
        name { Faker::Name.first_name }
        email { Faker::Internet.email }
    end
end