class PersonMailerPreview < ActionMailer::Preview
    def game_assignment_email
        PersonMailer.game_assignment_email(Person.first, Game.first)
    end
end
