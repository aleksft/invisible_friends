RSpec.describe Assignment, type: :model do
    describe '#validate' do
        before do
            @game = create(:game)
            @person1 = create(:person)
            @person2 = create(:person)
            @person3 = create(:person)
            @participant1 = create(:participant, game: @game, person: @person1)
            @participant2 = create(:participant, game: @game, person: @person2)
            @assignment = create(:assignment, game: @game, person: @person1, linked: @person2)
        end

        it 'creates an assignment' do
            Assignment.create!(game: @game, person: @person2, linked: @person1)

            expect(Assignment.last.game.id).to eq(@game.id)
            expect(Assignment.last.person.id).to eq(@person2.id)
            expect(Assignment.last.linked.id).to eq(@person1.id)
        end

        it 'fails with no game' do
            expect {
                Assignment.create!(person: @person1, linked: @person2)
            }.to raise_error(NoMethodError, "undefined method `participants' for nil:NilClass")
        end

        it 'fails with no valid game' do
            expect {
                Assignment.create!(game_id: -1, person: @person1, linked: @person2)
            }.to raise_error(NoMethodError, "undefined method `participants' for nil:NilClass")
        end

        it 'fails with no person' do
            expect {
                Assignment.create!(game: @game, linked: @person2)
            }.to raise_error(ActiveRecord::RecordInvalid, "Validation failed: Person can't be blank, Person must be a participant, Person must exist")
        end

        it 'fails with no valid person' do
            expect {
                Assignment.create!(game: @game, person: @person3, linked: @person2)
            }.to raise_error(ActiveRecord::RecordInvalid, "Validation failed: Person must be a participant")
        end

        it 'fails with no linked' do
            expect {
                Assignment.create!(game: @game, person: @person1)
            }.to raise_error(ActiveRecord::RecordInvalid, "Validation failed: Linked can't be blank, Linked must be a participant, Linked must exist")
        end

        it 'fails with no valid linked' do
            expect {
                Assignment.create!(game: @game, person: @person1, linked: @person3)
            }.to raise_error(ActiveRecord::RecordInvalid, "Validation failed: Linked must be a participant")
        end

        it 'fails with not unique' do
            expect {
                Assignment.create!(game: @game, person: @person1, linked: @person2)
            }.to raise_error(ActiveRecord::RecordNotUnique, "SQLite3::ConstraintException: UNIQUE constraint failed: assignments.game_id, assignments.person_id, assignments.linked_id")
        end

        it 'belongs to game' do
            expect(@assignment.game).not_to be_nil
        end

        it 'belongs to person' do
            expect(@assignment.person).not_to be_nil
        end

        it 'belongs to linked' do
            expect(@assignment.linked).not_to be_nil
        end
    end
end