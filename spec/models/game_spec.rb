RSpec.describe Game, type: :model do
    describe '#validate' do
        before do
            @game = create(:game)
            @person1 = create(:person)
            @person2 = create(:person)
            @person3 = create(:person)
            create(:participant, game: @game, person: @person1)
            create(:participant, game: @game, person: @person2)
            create(:participant, game: @game, person: @person3)
            create(:assignment, game: @game, person: @person1, linked: @person2)
            create(:assignment, game: @game, person: @person1, linked: @person3)
        end

        it 'creates a game' do
            Game.create!(name: 'Game', people_links: 1, min_price: 10, max_price: 20, date: Date.today)

            expect(Game.last.name).to eq('Game')
            expect(Game.last.people_links).to eq(1)
        end

        it 'fails with no name' do
            expect {
                Game.create!(people_links: 1, min_price: 10, max_price: 20, date: Date.today)
            }.to raise_error(ActiveRecord::RecordInvalid, "Validation failed: Name can't be blank")
        end

        it 'fails with no people_links' do
            expect {
                Game.create!(name: 'Game', min_price: 10, max_price: 20, date: Date.today)
            }.to raise_error(ActiveRecord::RecordInvalid, "Validation failed: People links can't be blank, People links is not a number")
        end

        it 'fails with no number people_links' do
            expect {
                Game.create!(name: 'Game', people_links: 'test', min_price: 10, max_price: 20, date: Date.today)
            }.to raise_error(ActiveRecord::RecordInvalid, "Validation failed: People links is not a number")
        end

        it 'fails with no int people_links' do
            expect {
                Game.create!(name: 'Game', people_links: 10.4, min_price: 10, max_price: 20, date: Date.today)
            }.to raise_error(ActiveRecord::RecordInvalid, "Validation failed: People links must be an integer")
        end

        it 'fails with wrong people_links' do
            expect {
                Game.create!(name: 'Game', people_links: 0, min_price: 10, max_price: 20, date: Date.today)
            }.to raise_error(ActiveRecord::RecordInvalid, "Validation failed: People links must be greater than or equal to 1")
        end

        it 'fails with no min_price' do
            expect {
                Game.create!(name: 'Game', people_links: 1, max_price: 20, date: Date.today)
            }.to raise_error(ActiveRecord::RecordInvalid, "Validation failed: Min price can't be blank, Min price is not a number")
        end

        it 'fails with no number min_price' do
            expect {
                Game.create!(name: 'Game', people_links: 1, min_price: 'test', max_price: 20, date: Date.today)
            }.to raise_error(ActiveRecord::RecordInvalid, "Validation failed: Min price is not a number")
        end

        it 'fails with no int min_price' do
            expect {
                Game.create!(name: 'Game', people_links: 1, min_price: 10.4, max_price: 20, date: Date.today)
            }.to raise_error(ActiveRecord::RecordInvalid, "Validation failed: Min price must be an integer")
        end

        it 'fails with wrong min_price' do
            expect {
                Game.create!(name: 'Game', people_links: 1, min_price: 0, max_price: 20, date: Date.today)
            }.to raise_error(ActiveRecord::RecordInvalid, "Validation failed: Min price must be greater than or equal to 1")
        end

        it 'fails with no max_price' do
            expect {
                Game.create!(name: 'Game', people_links: 1, min_price: 10, date: Date.today)
            }.to raise_error(ActiveRecord::RecordInvalid, "Validation failed: Max price can't be blank, Max price is not a number")
        end

        it 'fails with no number max_price' do
            expect {
                Game.create!(name: 'Game', people_links: 1, min_price: 10, max_price: 'test', date: Date.today)
            }.to raise_error(ActiveRecord::RecordInvalid, "Validation failed: Max price is not a number")
        end

        it 'fails with no int max_price' do
            expect {
                Game.create!(name: 'Game', people_links: 1, min_price: 10, max_price: 10.4, date: Date.today)
            }.to raise_error(ActiveRecord::RecordInvalid, "Validation failed: Max price must be an integer")
        end

        it 'fails with wrong max_price' do
            expect {
                Game.create!(name: 'Game', people_links: 1, min_price: 10, max_price: 9, date: Date.today)
            }.to raise_error(ActiveRecord::RecordInvalid, "Validation failed: Max price must be greater than or equal to 10")
        end

        it 'fails with no date' do
            expect {
                Game.create!(name: 'Game', people_links: 1, min_price: 10, max_price: 20)
            }.to raise_error(ActiveRecord::RecordInvalid, "Validation failed: Date can't be blank, Date is not a date")
        end

        it 'fails with no date date' do
            expect {
                Game.create!(name: 'Game', people_links: 1, min_price: 10, max_price: 20, date: 'test')
            }.to raise_error(ActiveRecord::RecordInvalid, "Validation failed: Date can't be blank, Date is not a date")
        end

        it 'has many participants' do
            expect(@game.participants.size).to eq(3)
        end

        it 'has many assignments' do
            expect(@game.assignments.size).to eq(2)
        end
    end

    describe '#add_participants' do
        before do
            @game = create(:game)

            @game1 = create(:game)
            @game2 = create(:game)
            @game3 = create(:game)
        end

        it 'creates people' do
            @game.add_participants([
                { name: 'Person 1', email: 'test1@example.com' },
                { name: 'Person 2', email: 'test2@example.com' },
                { name: 'Person 3', email: 'test3@example.com' }
            ])

            expect(@game.participants.size).to eq(3)
        end

        it 'fails without valid registries' do
            expect {
                @game.add_participants([
                    { name: 'Person 1' },
                    { email: 'test2@example.com' },
                    { name: 'Person 3', email: 'example.com' }
                ])
            }.to raise_error(ActiveRecord::RecordInvalid, "Validation failed: Email can't be blank, Email is invalid")
        end

        it 'does not duplicate existing people' do
            ref_email = 'test@example.com'

            @game1.add_participants([
                { name: 'Person', email: ref_email },
                { name: 'Person 1', email: 'test1@example.com' },
                { name: 'Person 2', email: 'test2@example.com' }
            ])

            person1 = Person.find_by_email(ref_email)
            expect(person1.name).to eq('Person')
            expect(person1.email).to eq(ref_email)

            expect(@game1.participants.where(person_id: person1.id).size).to eq(1)

            @game2.add_participants([
                { name: 'Ref. Person', email: ref_email },
                { name: 'Person 3', email: 'test3@example.com' },
                { name: 'Person 4', email: 'test4@example.com' }
            ])

            person2 = Person.find_by_email(ref_email)
            expect(person2.id).to eq(person1.id)
            expect(person2.name).to eq('Ref. Person')
            expect(person2.email).to eq(ref_email)

            expect(@game2.participants.where(person_id: person2.id).size).to eq(1)

            @game3.add_participants([
                { email: ref_email },
                { name: 'Person 5', email: 'test5@example.com' },
                { name: 'Person 6', email: 'test6@example.com' }
            ])

            person3 = Person.find_by_email(ref_email)
            expect(person3.id).to eq(person1.id)
            expect(person3.name).to eq('Ref. Person')
            expect(person3.email).to eq(ref_email)

            expect(@game3.participants.where(person_id: person3.id).size).to eq(1)
        end
    end

    describe '#assign' do
        before do
            @person1 = create(:person)
            @person2 = create(:person)
            @person3 = create(:person)
            @person4 = create(:person)
            @person5 = create(:person)
            @person6 = create(:person)
            @person7 = create(:person)
            @person8 = create(:person)
            @person9 = create(:person)

            @game1 = create(:game, people_links: 1)
            @participant11 = create(:participant, game: @game1, person: @person1)
            @participant12 = create(:participant, game: @game1, person: @person2)

            @game2 = create(:game, people_links: 2)
            @participant21 = create(:participant, game: @game2, person: @person1)
            @participant22 = create(:participant, game: @game2, person: @person2)
            @participant23 = create(:participant, game: @game2, person: @person3)
            @participant24 = create(:participant, game: @game2, person: @person4)
            @participant25 = create(:participant, game: @game2, person: @person5)
            @participant26 = create(:participant, game: @game2, person: @person6)
            @participant27 = create(:participant, game: @game2, person: @person7)
            @participant28 = create(:participant, game: @game2, person: @person8)
            @participant29 = create(:participant, game: @game2, person: @person9)

            @game3 = create(:game, people_links: 8)
            @participant31 = create(:participant, game: @game3, person: @person1)
            @participant32 = create(:participant, game: @game3, person: @person2)
            @participant33 = create(:participant, game: @game3, person: @person3)
            @participant34 = create(:participant, game: @game3, person: @person4)
            @participant35 = create(:participant, game: @game3, person: @person5)
            @participant36 = create(:participant, game: @game3, person: @person6)
            @participant37 = create(:participant, game: @game3, person: @person7)
            @participant38 = create(:participant, game: @game3, person: @person8)
            @participant39 = create(:participant, game: @game3, person: @person9)

            @game4 = create(:game, people_links: 20)
            (0..20).each do |i|
                person = create(:person, name: "Person #{i}", email: "test#{i}@example.com")
                create(:participant, game: @game4, person: person)
            end

            @game5 = create(:game, people_links: 2)
            create(:participant, game: @game5, person: @person1)
            create(:participant, game: @game5, person: @person2)
        end

        it 'creates assignments' do
            @game1.assign

            expect(@game1.errors.size).to eq(0)
            expect(@game1.assignments.size).to eq(2)

            expect(@game1.assignments.where(person: @person1).size).to eq(1)
            expect(@game1.assignments.where(person: @person2).size).to eq(1)

            expect(@game1.assignments.where(linked: @person1).size).to eq(1)
            expect(@game1.assignments.where(linked: @person2).size).to eq(1)

            expect(@game1.assignments.where(person: @person1, linked: @person1).size).to eq(0)
            expect(@game1.assignments.where(person: @person1, linked: @person2).size).to eq(1)

            expect(@game1.assignments.where(person: @person2, linked: @person1).size).to eq(1)
            expect(@game1.assignments.where(person: @person2, linked: @person2).size).to eq(0)
        end

        it 'creates multiple assignments' do
            @game2.assign

            expect(@game2.errors.size).to eq(0)
            expect(@game2.assignments.size).to eq(18)

            expect(@game2.assignments.where(person: @person1).size).to eq(2)
            expect(@game2.assignments.where(person: @person2).size).to eq(2)
            expect(@game2.assignments.where(person: @person3).size).to eq(2)
            expect(@game2.assignments.where(person: @person4).size).to eq(2)
            expect(@game2.assignments.where(person: @person5).size).to eq(2)
            expect(@game2.assignments.where(person: @person6).size).to eq(2)
            expect(@game2.assignments.where(person: @person7).size).to eq(2)
            expect(@game2.assignments.where(person: @person8).size).to eq(2)
            expect(@game2.assignments.where(person: @person9).size).to eq(2)

            expect(@game2.assignments.where(linked: @person1).size).to eq(2)
            expect(@game2.assignments.where(linked: @person2).size).to eq(2)
            expect(@game2.assignments.where(linked: @person3).size).to eq(2)
            expect(@game2.assignments.where(linked: @person4).size).to eq(2)
            expect(@game2.assignments.where(linked: @person5).size).to eq(2)
            expect(@game2.assignments.where(linked: @person6).size).to eq(2)
            expect(@game2.assignments.where(linked: @person7).size).to eq(2)
            expect(@game2.assignments.where(linked: @person8).size).to eq(2)
            expect(@game2.assignments.where(linked: @person9).size).to eq(2)

            expect(@game2.assignments.where(person: @person1, linked: @person1).size).to eq(0)
            expect(@game2.assignments.where(person: @person1, linked: @person2).size).to be <= 1
            expect(@game2.assignments.where(person: @person1, linked: @person3).size).to be <= 1
            expect(@game2.assignments.where(person: @person1, linked: @person4).size).to be <= 1
            expect(@game2.assignments.where(person: @person1, linked: @person5).size).to be <= 1
            expect(@game2.assignments.where(person: @person1, linked: @person6).size).to be <= 1
            expect(@game2.assignments.where(person: @person1, linked: @person7).size).to be <= 1
            expect(@game2.assignments.where(person: @person1, linked: @person8).size).to be <= 1
            expect(@game2.assignments.where(person: @person1, linked: @person9).size).to be <= 1

            expect(@game2.assignments.where(person: @person2, linked: @person1).size).to be <= 1
            expect(@game2.assignments.where(person: @person2, linked: @person2).size).to eq(0)
            expect(@game2.assignments.where(person: @person2, linked: @person3).size).to be <= 1
            expect(@game2.assignments.where(person: @person2, linked: @person4).size).to be <= 1
            expect(@game2.assignments.where(person: @person2, linked: @person5).size).to be <= 1
            expect(@game2.assignments.where(person: @person2, linked: @person6).size).to be <= 1
            expect(@game2.assignments.where(person: @person2, linked: @person7).size).to be <= 1
            expect(@game2.assignments.where(person: @person2, linked: @person8).size).to be <= 1
            expect(@game2.assignments.where(person: @person2, linked: @person9).size).to be <= 1

            expect(@game2.assignments.where(person: @person3, linked: @person1).size).to be <= 1
            expect(@game2.assignments.where(person: @person3, linked: @person2).size).to be <= 1
            expect(@game2.assignments.where(person: @person3, linked: @person3).size).to eq(0)
            expect(@game2.assignments.where(person: @person3, linked: @person4).size).to be <= 1
            expect(@game2.assignments.where(person: @person3, linked: @person5).size).to be <= 1
            expect(@game2.assignments.where(person: @person3, linked: @person6).size).to be <= 1
            expect(@game2.assignments.where(person: @person3, linked: @person7).size).to be <= 1
            expect(@game2.assignments.where(person: @person3, linked: @person8).size).to be <= 1
            expect(@game2.assignments.where(person: @person3, linked: @person9).size).to be <= 1

            expect(@game2.assignments.where(person: @person4, linked: @person1).size).to be <= 1
            expect(@game2.assignments.where(person: @person4, linked: @person2).size).to be <= 1
            expect(@game2.assignments.where(person: @person4, linked: @person3).size).to be <= 1
            expect(@game2.assignments.where(person: @person4, linked: @person4).size).to eq(0)
            expect(@game2.assignments.where(person: @person4, linked: @person5).size).to be <= 1
            expect(@game2.assignments.where(person: @person4, linked: @person6).size).to be <= 1
            expect(@game2.assignments.where(person: @person4, linked: @person7).size).to be <= 1
            expect(@game2.assignments.where(person: @person4, linked: @person8).size).to be <= 1
            expect(@game2.assignments.where(person: @person4, linked: @person9).size).to be <= 1

            expect(@game2.assignments.where(person: @person5, linked: @person1).size).to be <= 1
            expect(@game2.assignments.where(person: @person5, linked: @person2).size).to be <= 1
            expect(@game2.assignments.where(person: @person5, linked: @person3).size).to be <= 1
            expect(@game2.assignments.where(person: @person5, linked: @person4).size).to be <= 1
            expect(@game2.assignments.where(person: @person5, linked: @person5).size).to eq(0)
            expect(@game2.assignments.where(person: @person5, linked: @person6).size).to be <= 1
            expect(@game2.assignments.where(person: @person5, linked: @person7).size).to be <= 1
            expect(@game2.assignments.where(person: @person5, linked: @person8).size).to be <= 1
            expect(@game2.assignments.where(person: @person5, linked: @person9).size).to be <= 1

            expect(@game2.assignments.where(person: @person6, linked: @person1).size).to be <= 1
            expect(@game2.assignments.where(person: @person6, linked: @person2).size).to be <= 1
            expect(@game2.assignments.where(person: @person6, linked: @person3).size).to be <= 1
            expect(@game2.assignments.where(person: @person6, linked: @person4).size).to be <= 1
            expect(@game2.assignments.where(person: @person6, linked: @person5).size).to be <= 1
            expect(@game2.assignments.where(person: @person6, linked: @person6).size).to eq(0)
            expect(@game2.assignments.where(person: @person6, linked: @person7).size).to be <= 1
            expect(@game2.assignments.where(person: @person6, linked: @person8).size).to be <= 1
            expect(@game2.assignments.where(person: @person6, linked: @person9).size).to be <= 1

            expect(@game2.assignments.where(person: @person7, linked: @person1).size).to be <= 1
            expect(@game2.assignments.where(person: @person7, linked: @person2).size).to be <= 1
            expect(@game2.assignments.where(person: @person7, linked: @person3).size).to be <= 1
            expect(@game2.assignments.where(person: @person7, linked: @person4).size).to be <= 1
            expect(@game2.assignments.where(person: @person7, linked: @person5).size).to be <= 1
            expect(@game2.assignments.where(person: @person7, linked: @person6).size).to be <= 1
            expect(@game2.assignments.where(person: @person7, linked: @person7).size).to eq(0)
            expect(@game2.assignments.where(person: @person7, linked: @person8).size).to be <= 1
            expect(@game2.assignments.where(person: @person7, linked: @person9).size).to be <= 1

            expect(@game2.assignments.where(person: @person8, linked: @person1).size).to be <= 1
            expect(@game2.assignments.where(person: @person8, linked: @person2).size).to be <= 1
            expect(@game2.assignments.where(person: @person8, linked: @person3).size).to be <= 1
            expect(@game2.assignments.where(person: @person8, linked: @person4).size).to be <= 1
            expect(@game2.assignments.where(person: @person8, linked: @person5).size).to be <= 1
            expect(@game2.assignments.where(person: @person8, linked: @person6).size).to be <= 1
            expect(@game2.assignments.where(person: @person8, linked: @person7).size).to be <= 1
            expect(@game2.assignments.where(person: @person8, linked: @person8).size).to eq(0)
            expect(@game2.assignments.where(person: @person8, linked: @person9).size).to be <= 1

            expect(@game2.assignments.where(person: @person9, linked: @person1).size).to be <= 1
            expect(@game2.assignments.where(person: @person9, linked: @person2).size).to be <= 1
            expect(@game2.assignments.where(person: @person9, linked: @person3).size).to be <= 1
            expect(@game2.assignments.where(person: @person9, linked: @person4).size).to be <= 1
            expect(@game2.assignments.where(person: @person9, linked: @person5).size).to be <= 1
            expect(@game2.assignments.where(person: @person9, linked: @person6).size).to be <= 1
            expect(@game2.assignments.where(person: @person9, linked: @person7).size).to be <= 1
            expect(@game2.assignments.where(person: @person9, linked: @person8).size).to be <= 1
            expect(@game2.assignments.where(person: @person9, linked: @person9).size).to eq(0)
        end

        it 'creates all multiple assignments' do
            @game3.assign

            expect(@game3.errors.size).to eq(0)
            expect(@game3.assignments.size).to eq(72)

            expect(@game3.assignments.where(person: @person1).size).to eq(8)
            expect(@game3.assignments.where(person: @person2).size).to eq(8)
            expect(@game3.assignments.where(person: @person3).size).to eq(8)
            expect(@game3.assignments.where(person: @person4).size).to eq(8)
            expect(@game3.assignments.where(person: @person5).size).to eq(8)
            expect(@game3.assignments.where(person: @person6).size).to eq(8)
            expect(@game3.assignments.where(person: @person7).size).to eq(8)
            expect(@game3.assignments.where(person: @person8).size).to eq(8)
            expect(@game3.assignments.where(person: @person9).size).to eq(8)

            expect(@game3.assignments.where(linked: @person1).size).to eq(8)
            expect(@game3.assignments.where(linked: @person2).size).to eq(8)
            expect(@game3.assignments.where(linked: @person3).size).to eq(8)
            expect(@game3.assignments.where(linked: @person4).size).to eq(8)
            expect(@game3.assignments.where(linked: @person5).size).to eq(8)
            expect(@game3.assignments.where(linked: @person6).size).to eq(8)
            expect(@game3.assignments.where(linked: @person7).size).to eq(8)
            expect(@game3.assignments.where(linked: @person8).size).to eq(8)
            expect(@game3.assignments.where(linked: @person9).size).to eq(8)

            expect(@game3.assignments.where(person: @person1, linked: @person1).size).to eq(0)
            expect(@game3.assignments.where(person: @person1, linked: @person2).size).to eq(1)
            expect(@game3.assignments.where(person: @person1, linked: @person3).size).to eq(1)
            expect(@game3.assignments.where(person: @person1, linked: @person4).size).to eq(1)
            expect(@game3.assignments.where(person: @person1, linked: @person5).size).to eq(1)
            expect(@game3.assignments.where(person: @person1, linked: @person6).size).to eq(1)
            expect(@game3.assignments.where(person: @person1, linked: @person7).size).to eq(1)
            expect(@game3.assignments.where(person: @person1, linked: @person8).size).to eq(1)
            expect(@game3.assignments.where(person: @person1, linked: @person9).size).to eq(1)

            expect(@game3.assignments.where(person: @person2, linked: @person1).size).to eq(1)
            expect(@game3.assignments.where(person: @person2, linked: @person2).size).to eq(0)
            expect(@game3.assignments.where(person: @person2, linked: @person3).size).to eq(1)
            expect(@game3.assignments.where(person: @person2, linked: @person4).size).to eq(1)
            expect(@game3.assignments.where(person: @person2, linked: @person5).size).to eq(1)
            expect(@game3.assignments.where(person: @person2, linked: @person6).size).to eq(1)
            expect(@game3.assignments.where(person: @person2, linked: @person7).size).to eq(1)
            expect(@game3.assignments.where(person: @person2, linked: @person8).size).to eq(1)
            expect(@game3.assignments.where(person: @person2, linked: @person9).size).to eq(1)

            expect(@game3.assignments.where(person: @person3, linked: @person1).size).to eq(1)
            expect(@game3.assignments.where(person: @person3, linked: @person2).size).to eq(1)
            expect(@game3.assignments.where(person: @person3, linked: @person3).size).to eq(0)
            expect(@game3.assignments.where(person: @person3, linked: @person4).size).to eq(1)
            expect(@game3.assignments.where(person: @person3, linked: @person5).size).to eq(1)
            expect(@game3.assignments.where(person: @person3, linked: @person6).size).to eq(1)
            expect(@game3.assignments.where(person: @person3, linked: @person7).size).to eq(1)
            expect(@game3.assignments.where(person: @person3, linked: @person8).size).to eq(1)
            expect(@game3.assignments.where(person: @person3, linked: @person9).size).to eq(1)

            expect(@game3.assignments.where(person: @person4, linked: @person1).size).to eq(1)
            expect(@game3.assignments.where(person: @person4, linked: @person2).size).to eq(1)
            expect(@game3.assignments.where(person: @person4, linked: @person3).size).to eq(1)
            expect(@game3.assignments.where(person: @person4, linked: @person4).size).to eq(0)
            expect(@game3.assignments.where(person: @person4, linked: @person5).size).to eq(1)
            expect(@game3.assignments.where(person: @person4, linked: @person6).size).to eq(1)
            expect(@game3.assignments.where(person: @person4, linked: @person7).size).to eq(1)
            expect(@game3.assignments.where(person: @person4, linked: @person8).size).to eq(1)
            expect(@game3.assignments.where(person: @person4, linked: @person9).size).to eq(1)

            expect(@game3.assignments.where(person: @person5, linked: @person1).size).to eq(1)
            expect(@game3.assignments.where(person: @person5, linked: @person2).size).to eq(1)
            expect(@game3.assignments.where(person: @person5, linked: @person3).size).to eq(1)
            expect(@game3.assignments.where(person: @person5, linked: @person4).size).to eq(1)
            expect(@game3.assignments.where(person: @person5, linked: @person5).size).to eq(0)
            expect(@game3.assignments.where(person: @person5, linked: @person6).size).to eq(1)
            expect(@game3.assignments.where(person: @person5, linked: @person7).size).to eq(1)
            expect(@game3.assignments.where(person: @person5, linked: @person8).size).to eq(1)
            expect(@game3.assignments.where(person: @person5, linked: @person9).size).to eq(1)

            expect(@game3.assignments.where(person: @person6, linked: @person1).size).to eq(1)
            expect(@game3.assignments.where(person: @person6, linked: @person2).size).to eq(1)
            expect(@game3.assignments.where(person: @person6, linked: @person3).size).to eq(1)
            expect(@game3.assignments.where(person: @person6, linked: @person4).size).to eq(1)
            expect(@game3.assignments.where(person: @person6, linked: @person5).size).to eq(1)
            expect(@game3.assignments.where(person: @person6, linked: @person6).size).to eq(0)
            expect(@game3.assignments.where(person: @person6, linked: @person7).size).to eq(1)
            expect(@game3.assignments.where(person: @person6, linked: @person8).size).to eq(1)
            expect(@game3.assignments.where(person: @person6, linked: @person9).size).to eq(1)

            expect(@game3.assignments.where(person: @person7, linked: @person1).size).to eq(1)
            expect(@game3.assignments.where(person: @person7, linked: @person2).size).to eq(1)
            expect(@game3.assignments.where(person: @person7, linked: @person3).size).to eq(1)
            expect(@game3.assignments.where(person: @person7, linked: @person4).size).to eq(1)
            expect(@game3.assignments.where(person: @person7, linked: @person5).size).to eq(1)
            expect(@game3.assignments.where(person: @person7, linked: @person6).size).to eq(1)
            expect(@game3.assignments.where(person: @person7, linked: @person7).size).to eq(0)
            expect(@game3.assignments.where(person: @person7, linked: @person8).size).to eq(1)
            expect(@game3.assignments.where(person: @person7, linked: @person9).size).to eq(1)

            expect(@game3.assignments.where(person: @person8, linked: @person1).size).to eq(1)
            expect(@game3.assignments.where(person: @person8, linked: @person2).size).to eq(1)
            expect(@game3.assignments.where(person: @person8, linked: @person3).size).to eq(1)
            expect(@game3.assignments.where(person: @person8, linked: @person4).size).to eq(1)
            expect(@game3.assignments.where(person: @person8, linked: @person5).size).to eq(1)
            expect(@game3.assignments.where(person: @person8, linked: @person6).size).to eq(1)
            expect(@game3.assignments.where(person: @person8, linked: @person7).size).to eq(1)
            expect(@game3.assignments.where(person: @person8, linked: @person8).size).to eq(0)
            expect(@game3.assignments.where(person: @person8, linked: @person9).size).to eq(1)

            expect(@game3.assignments.where(person: @person9, linked: @person1).size).to eq(1)
            expect(@game3.assignments.where(person: @person9, linked: @person2).size).to eq(1)
            expect(@game3.assignments.where(person: @person9, linked: @person3).size).to eq(1)
            expect(@game3.assignments.where(person: @person9, linked: @person4).size).to eq(1)
            expect(@game3.assignments.where(person: @person9, linked: @person5).size).to eq(1)
            expect(@game3.assignments.where(person: @person9, linked: @person6).size).to eq(1)
            expect(@game3.assignments.where(person: @person9, linked: @person7).size).to eq(1)
            expect(@game3.assignments.where(person: @person9, linked: @person8).size).to eq(1)
            expect(@game3.assignments.where(person: @person9, linked: @person9).size).to eq(0)
        end

        it 'fails with too much retries' do
            @game4.assign

            expect(@game4.errors.size).to eq(1)
            expect(@game4.errors.first.type).to eq("Too much retries")
            expect(@game4.assignments.size).to eq(0)
        end

        it 'fails with people_links greater than or equal number of people' do
            @game5.assign

            expect(@game5.errors.size).to eq(1)
            expect(@game5.errors.first.type).to eq("Participants links can't be greater or equal than number of people")
            expect(@game5.assignments.size).to eq(0)
        end
    end
end