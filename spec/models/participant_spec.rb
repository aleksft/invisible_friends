RSpec.describe Participant, type: :model do
    describe '#validate' do
        before do
            @game = create(:game)
            @person = create(:person)
            @participant = create(:participant, game: @game, person: @person)

            @game1 = create(:game)
            @person1 = create(:person)

            @person2 = create(:person)

            @game3 = create(:game)
        end

        it 'creates a participant' do
            Participant.create!(game: @game1, person: @person1)

            expect(Participant.last.game.id).to eq(@game1.id)
            expect(Participant.last.person.id).to eq(@person1.id)
        end

        it 'fails with no game' do
            expect {
                Participant.create!(person: @person2)
            }.to raise_error(ActiveRecord::RecordInvalid, "Validation failed: Game can't be blank, Game must exist")
        end

        it 'fails with no valid game' do
            expect {
                Participant.create!(game_id: -1, person: @person)
            }.to raise_error(ActiveRecord::RecordInvalid, "Validation failed: Game can't be blank, Game must exist")
        end

        it 'fails with no person' do
            expect {
                Participant.create!(game: @game3)
            }.to raise_error(ActiveRecord::RecordInvalid, "Validation failed: Person can't be blank, Person must exist")
        end

        it 'fails with no valid person' do
            expect {
                Participant.create!(game: @game, person_id: -1)
            }.to raise_error(ActiveRecord::RecordInvalid, "Validation failed: Person can't be blank, Person must exist")
        end

        it 'belongs to game' do
            expect(@participant.game).not_to be_nil
        end

        it 'belongs to person' do
            expect(@participant.person).not_to be_nil
        end
    end
end