RSpec.describe Person, type: :model do
    describe '#validate' do
        before do
            @person1 = create(:person)
            @person2 = create(:person)
            @person3 = create(:person)

            game = create(:game)
            create(:participant, game: game, person: @person1)
            create(:participant, game: game, person: @person2)
            create(:participant, game: game, person: @person3)
            create(:assignment, game: game, person: @person1, linked: @person2)
            create(:assignment, game: game, person: @person2, linked: @person1)
            create(:assignment, game: game, person: @person3, linked: @person1)
        end

        it 'creates a person' do
            Person.create!(name: 'Person', email: 'test@example.com')

            expect(Person.last.name).to eq('Person')
            expect(Person.last.email).to eq('test@example.com')
        end

        it 'fails with no name' do
            expect {
                Person.create!(email: 'test@example.com')
            }.to raise_error(ActiveRecord::RecordInvalid, "Validation failed: Name can't be blank")
        end

        it 'fails with no email' do
            expect {
                Person.create!(name: 'Person')
            }.to raise_error(ActiveRecord::RecordInvalid, "Validation failed: Email can't be blank, Email is invalid")
        end

        it 'fails with duplicated email' do
            expect {
                Person.create!(name: 'Person 1', email: 'test@example.com')
                Person.create!(name: 'Person 2', email: 'test@example.com')
            }.to raise_error(ActiveRecord::RecordInvalid, "Validation failed: Email has already been taken")
        end

        it 'fails with no valid email' do
            expect {
                Person.create!(name: 'Person', email: 'example.com')
            }.to raise_error(ActiveRecord::RecordInvalid, "Validation failed: Email is invalid")
        end

        it 'has many assignments' do
            expect(@person1.assignments.size).to eq(1)
        end

        it 'has many links' do
            expect(@person1.links.size).to eq(2)
        end
    end
end